import { INews } from '../interfaces/inews';

export class DummyData {
    dummyNews: INews = {
        __v: 0,
        _id: '',
        author: '',
        new_date: '',
        title: '',
        url: ''
    }
}