export interface INews {
  _id: string;
  title: string;
  url: string;
  author: string;
  new_date: string;
  __v: number;
  created_at?: string;
  tags?: string[];
}
