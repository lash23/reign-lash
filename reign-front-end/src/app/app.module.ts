import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { NewsListComponent } from './modules/news-list/news-list.component';
import { NewsListModule } from './modules/news-list/news-list.module';
import { FooterComponent } from './shared/footer/footer.component';

import { NgHttpLoaderModule } from 'ng-http-loader';
import { TooltipModule } from 'ng2-tooltip-directive';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NewsListComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    NewsListModule,
    HttpClientModule,
    NgHttpLoaderModule.forRoot(),
    TooltipModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
