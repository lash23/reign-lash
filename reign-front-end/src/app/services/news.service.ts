import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { INews } from '../interfaces/inews';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  url_base = environment.backendUrl

  constructor(
    private http: HttpClient
  ) { }

  getNews() {
    return this.http.get(`${this.url_base}/notices/get-all`)
  }

  deleteNew(id: string) {
    return this.http.delete(`${this.url_base}/notices/hard-delete/${id}`)
  }
}
