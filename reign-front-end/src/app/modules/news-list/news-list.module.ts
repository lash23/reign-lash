import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NewsCardComponent } from './news-card/news-card.component';

import { TooltipModule } from 'ng2-tooltip-directive';
import { DummyData } from 'src/app/utils/dummy.data';

@NgModule({
  declarations: [NewsCardComponent],
  exports: [NewsCardComponent],
  imports: [
    CommonModule,
    BrowserModule,
    TooltipModule
  ],
  providers: [DummyData]
})
export class NewsListModule { }
