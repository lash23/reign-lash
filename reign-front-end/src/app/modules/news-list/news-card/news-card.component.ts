import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { INews } from 'src/app/interfaces/inews';
import { NewsService } from 'src/app/services/news.service';
import { DummyData } from 'src/app/utils/dummy.data';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.css']
})
export class NewsCardComponent implements OnInit {
  @Input() new: INews = this.dummyData.dummyNews;
  @Output() newDeletedEvent = new EventEmitter<string>();

  constructor(
    private newsService: NewsService,
    private dummyData: DummyData
  ) { }

  ngOnInit(): void {
  }

  delete(id: string) {
    Swal.fire({
      title: 'sure you want to delete it?',
      text: 'You will not be able to get it back later',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.newsService.deleteNew(id).subscribe( (res: any) => {
          if( res.ok) {
            this.newDeletedEvent.emit(id);
            Swal.fire(
              'Deleted!',
              'Successfuly deleted.',
              'success'
            )
          } else {
            Swal.fire(
              'Cancelled',
              'The news has not been deleted',
              'error'
            )
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'The news has not been deleted',
          'error'
        )
      }
    })
  }

  browse(url: string) {
    if (!url) {
      Swal.fire(
        'Sorry!',
        'this news has no url.',
        'warning'
      )
    } else {
      window.open(url, '_blank')
    }
  }
}
