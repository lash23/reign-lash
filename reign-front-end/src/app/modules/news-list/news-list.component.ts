import { Component, OnInit } from '@angular/core';
import { INews } from 'src/app/interfaces/inews';
import { NewsService } from 'src/app/services/news.service';
import { DummyData } from 'src/app/utils/dummy.data';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.css']
})
export class NewsListComponent implements OnInit {
  constructor(
    private newsService: NewsService,
    private dummyData: DummyData
  ) { }
  news: INews[] = [this.dummyData.dummyNews];

  ngOnInit(): void {
    this.getAllNews();
  }

  getAllNews() {
    this.newsService.getNews().subscribe( (res: any) => {
      this.news = res.data
    })
  }

  deleteNews(id: string) {
    this.getAllNews();
    this.news.filter( _new => _new._id != id);
  }
}
