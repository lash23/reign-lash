# Reign && Lash test
Web application that is responsible for obtaining news with the theme of programming. The news are obtained programmatically every hour and can be viewed from the front end of the application.

The application is composed of a Back end that provides the API necessary to obtain the news from the database. The back end is located in the reign-back-end folder and is mainly made up of NestJS and MongoDB technologies.

The other half of the application is completed by a front end client, located in the reign-front-end folder, which has been built mainly with Angular 11.

## Launch de app with Docker Compose
Once you have cloned the repository with 
```bash
    git clone https://gitlab.com/lash23/reign-lash.git
```

navigate to the project folder:
```bash
    cd reign-lash
```
here you should build the project using the command
```bash
    docker-compose build
```
then raise the application with the command
```bash
    docker-compose up
```

Now the application is available, to see it visit from your browser
http://localhost:4201

## API Documentation
You can visit a detailed documentation on the endpoints and schemes provided by the API of this application through
http://localhost:3000/api/docs/v1/