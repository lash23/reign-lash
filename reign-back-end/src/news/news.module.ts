import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NewsUtils } from 'src/utils/utils.class';
import { NewSchema } from '../schemas/new.schema';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'New',
        schema: NewSchema
      }
    ]),
    HttpModule
  ],
  controllers: [NewsController],
  providers: [NewsService, NewsUtils],
  exports: [NewsService]
})
export class NewsModule {}
