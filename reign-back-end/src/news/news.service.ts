import { HttpService, Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AxiosResponse } from "axios";
import { New, NewDocument } from '../schemas/new.schema';
import { INewHit } from 'src/interfaces/new.interface';
import { NewsUtils } from 'src/utils/utils.class';

@Injectable()
export class NewsService {
  constructor(
    @InjectModel('New') private newModel: Model<NewDocument>,
    private httpService: HttpService,
    private newsUtils: NewsUtils
    ) {}

  // Fetch news from remote source
  fetchNews():  Observable<AxiosResponse>{
    return this.httpService.get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs').pipe(
      map(res => {
        // Save in DB frst
        res.data.hits.forEach( hit => this.createNewHit(hit));
        return res.data
      })
    )
  }

  // Get all new from DB
  async getAll(): Promise<New[]> {
    const news = await this.newModel.find().sort({original_date: -1});
    return news;
  }

  // Create new in DB
  async createNewHit(hit: INewHit): Promise<New> {
    const mappedHit = this.mapHit(hit);
    const newHit = new this.newModel(mappedHit);
    return await newHit.save();
  }

  // Delete new by ID
  async newHardDelete(newId: string): Promise<New> {
    const deletedNew = await this.newModel.findByIdAndDelete(newId);
    return deletedNew;
  }

  // map new hit
  mapHit(hit: INewHit) {
    const mappedHit = {
      title: hit.title || hit.story_title,
      url: hit.url,
      author: hit.author,
      new_date: this.newsUtils.formatNewsDate(hit.created_at),
      original_date: hit.created_at,
      tags: hit._tags,
    }
    return mappedHit
  }

  // get news every 1 hour
  @Cron('0 0 */1 * * *')
  newsCron() {
    const date = new Date();
    this.fetchNews().subscribe( res => console.log(`fetching news @${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`,res));
  }
}
