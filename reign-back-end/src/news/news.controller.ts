import { Controller, Delete, Get, HttpStatus, NotFoundException, Param, Res } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { NewsGetResponseDto } from 'src/dto/new.dto';
import { NewsService } from './news.service';

@ApiTags('notices')
@Controller('notices')
export class NewsController {
  constructor( private newsService: NewsService) {}

  @ApiOkResponse({
    description: 'Get original news from external source and store in database'
  })
  @Get('fetch')
  async fetchNewsSource(@Res() res: Response) {
    this.newsService.fetchNews().subscribe( (data: any) => {
      return res.status(HttpStatus.OK).json({
        message: 'news source successfuly listed',
        data,
        ok: true
      })
    })
  }

  @ApiOkResponse({
    description: 'Returns all the news stored in the database',
    type: NewsGetResponseDto
  })
  @Get('get-all')
  async findAllNews(@Res() res: Response) {
    const data = await this.newsService.getAll();
    return res.status(HttpStatus.OK).json({
      message: 'all news was successfuly listed',
      data,
      ok: true
    })
  }

  @ApiOkResponse({
    description: 'Permanently delete a news from the database. It cannot be recovered unless the source sends it back.'
  })
  @Delete('hard-delete/:id')
  async deleteNew(@Param('id') id: string, @Res() res: Response) {
    const data = await this.newsService.newHardDelete(id);
    if (!data) throw new NotFoundException('New not found');
    return res.status(HttpStatus.OK).json({
      message: 'new was successfuly deleted',
      data,
      ok: true
    })
  }
}
