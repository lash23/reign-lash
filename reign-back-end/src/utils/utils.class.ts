import * as moment from 'moment';

export class NewsUtils {
    formatNewsDate(news_date: string): string {
        const local = moment().local();
        const news_date_ilf = moment(news_date).local() // news date in local format;
        const today = local.clone().startOf('day');
        const yesterday = local.clone().subtract(1, 'days').startOf('day');
        const news_date_ilfs = moment(news_date).format('YYYY-MM-DD'); // news date in local && simple format;
        if (moment(news_date_ilfs).isSame(today, 'd')) { 
            return moment(news_date_ilf).format('hh:mm');
        } else if((moment(news_date_ilfs).isSame(yesterday, 'd'))) {
            return 'Yesterday'; // By Paul McCartney in 1965;
        } else {
            return moment(news_date_ilfs).format('MMM DD');
        }
    }
}
