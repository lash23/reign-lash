import { ApiProperty } from "@nestjs/swagger";

export class NewDto {
  @ApiProperty()
  readonly title: string;
  @ApiProperty()
  readonly author: string;
  @ApiProperty()
  readonly url: string;
  @ApiProperty()
  readonly notice_date: string;
  @ApiProperty()
  readonly tags: string[];
}

export class NewsGetResponseDto {
  @ApiProperty()
  readonly status: any;
  @ApiProperty()
  readonly message: string;
  @ApiProperty()
  readonly data: NewDto;
  @ApiProperty()
  readonly ok: boolean;
}
