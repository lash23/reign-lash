import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type NewDocument = New & Document;

@Schema()
export class New {

  @Prop()
  title: String;

  @Prop()
  url: String;

  @Prop()
  author: String;

  @Prop()
  new_date: String;

  @Prop()
  original_date: String;

  @Prop([String])
  tags: String[];

  @Prop({default: Date.now()})
  created_at: Date;
}

export const NewSchema = SchemaFactory.createForClass(New);
