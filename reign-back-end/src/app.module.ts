import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NewsModule } from './news/news.module';
import { NewsService } from './news/news.service';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://mongo/reign-lash', { useFindAndModify: false }),
    NewsModule,
    ScheduleModule.forRoot()
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(
    private newsService: NewsService
  ) {
    this.newsService.fetchNews().subscribe( res => console.log('fetching news for first time', res));
  }
}
