import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');
  app.enableCors();

  const options = new DocumentBuilder()
  .setTitle('Reign Test API')
  .setDescription('A simple Restfull app with testing purpose')
  .setVersion('1.0')
  .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/docs/v1', app, document);

  const server = await app.listen(3000);
  server.setTimeout(60000);
}
bootstrap();
