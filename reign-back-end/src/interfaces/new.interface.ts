export interface INew {
  hits: INewHit[],
  nbHits: number,
  page: number,
  nbPages: number,
  hitsPerPage: number,
  exhaustiveNbHits: boolean,
  query: string,
  params: string,
  processingTimeMS: number
}

export interface INewHit {
  created_at: string,
  title?: string,
  url: string,
  author: string,
  points: number,
  story_text: string,
  comment_text?: string,
  num_comments: number,
  story_id: number,
  story_title?: string,
  story_url: string,
  parent_id: number,
  created_at_i: number,
  _tags: string[],
  objectID: string,
  _highlightResult: IHitHighlight
}

export interface IHitHighlight {
  author: IHighlightKey,
  comment_text?: IHighlightKey, 
  story_title?: IHighlightKey
  title?: IHighlightKey,
  url: IHighlightKey
}

export interface IHighlightKey {
  value: string,
  matchLevel?: string,
  fullyHighlighted?: boolean,
  matchedWords?: string[]
}
